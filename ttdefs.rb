# Copyright (c) 2008 Charles Bachhuber
# Licensed under the MIT License, see license.txt for details.

module TTDefs
  HOST = "76.88.68.174"
  PORT = 56310
  MSGS = {
    :mBugged    => 0x00,
    :mConnect   => 0x01,
    :mDiscon    => 0x02,
    :mLog       => 0x03,
    :mIAm       => 0x04,
    :mSay       => 0x05,
    :mOSay      => 0x06,
    :mCalc      => 0x07,
    :mRoll      => 0x08,
    :mFact      => 0x09,
    :mCheck     => 0x0A,
    :mMapUp     => 0x0B,
    :mMapClear  => 0x0C,
    :mUplink    => 0x0D,
    :mPointer   => 0x0E,
    :mATData    => 0x0F,
    :mNextAT    => 0x10,

    :mVersion   => 0xEE,
    :mSetRoom   => 0xEF,

    :mBatch     => 0xFE
  }
  LMSGS = {
    :lPresent   => -1
  }
  
  PRINTEMS = [:mSay, :mOSay, :mCalc, :mRoll, :mFact, :mCheck]
end