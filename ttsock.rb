# Copyright (c) 2008 Charles Bachhuber
# Licensed under the MIT License, see license.txt for details.

require 'ttdefs'
require 'socket'
require 'thread'

class TTSock
  
  def initialize(host, room, name)
    @host = host
    @room = room
    @name = name
    @userlist = []
    
    connect()
    start_reader()
  end
  
  def msg(msgtype, payload)
    @socket << [4 + payload.length, TTDefs::MSGS[msgtype], payload].pack("VVA" + payload.length.to_s)
  end
  
  def start_reader()
    @queue = Queue.new
    Thread.new() do
      while @connected
        select([@socket])
        packet = @socket.recv(@socket.recv(4).unpack("V")[0])
        msgtype = packet.unpack("V")[0]
        payload = packet.slice(4..packet.length)
        @queue << [TTDefs::MSGS.invert[msgtype], payload]
      end
    end
  end
  
  def connect()
    unless @connected
      @socket = TCPSocket.new(@host, TTDefs::PORT)
      msg(:mSetRoom, @room)
      msg(:mConnect, @name)
      @connected = true;
    end
  end
  
  def disconnect()
    if @connected
      msg(:mDiscon, @name)
      @socket.close()
      @connected = false
      @queue << [:mFact, "Disconnected: Please exit now."]
    end
  end
  
  def get_msg()
    if (!@queue or @queue.length == 0)
      return nil
    else
      msg = @queue.pop
      if (msg[0] == :mConnect)
        @userlist.clear
        msg(:mIAm, @name)
        @userlist << msg[1]
        @userlist.sort!
        msg = [:mFact, msg[1] + " connected"]
      end
      if (msg[0] == :mIAm)
        @userlist << msg[1]
        @userlist.sort!
      end
      if (msg[0] == :mLog)
        oldlength = msg[1][1, 4].unpack("v")[0]
        newlength = msg[1][5 + oldlength, 4].unpack("v")[0]
        oldname = msg[1][5, oldlength]
        newname = msg[1][5 + oldlength + 4, newlength]
        @userlist.delete_at(@userlist.index(oldname))
        @userlist << newname
        @userlist.sort!
        msg = [:mFact, oldname + " is now known as " + newname]
      end
      if (msg[0] == :mDiscon)
        @userlist.delete_at(@userlist.index(msg[1]))
        msg = [:mFact, msg[1] + " disconnected"]
      end
      return msg
    end
  end
  
  def list_users()
    loopback(:mFact, "Users: " + @userlist.join("; "))
  end
  
  def name=(name)
    if @connected
      oldname = @name
      newname = name
      oldlength = oldname.length
      newlength = newname.length
      msg(:mLog, [oldlength, oldname, newlength, newname].pack("xVa" + oldlength.to_s + "Va" + newlength.to_s))
      loopback(:mFact, "You are now known as " + newname + ".")
      @name = name
    end
  end
  
  def loopback(msgtype, payload)
    @queue << [msgtype, payload]
  end
  
  def msgl(msgtype, payload)
    msg(msgtype, payload)
    loopback(msgtype, payload)
  end
  
  attr_reader :name, :room
end

class TTCommander
  def initialize(sock)
    @sock = sock
  end

  
  def xyzzy_do_cmd(cmdstr)
    if (cmdstr[0].chr == "/")
      foo = cmdstr.split(/\s/, 2)
      send(foo[0][1..-1].to_sym, foo[1])
    else
      send(:ooc, cmdstr)
    end
  end
  
  def ooc(str)
    @sock.msgl(:mOSay, @sock.name + "(OOC): " + str)
  end
  
  def ic(str)
    @sock.msgl(:mSay, @sock.name + ": " + str)
  end
  
  def fact(str)
    @sock.msgl(:mFact, str)
  end
  
  def me(str)
    fact(@sock.name + "(OOC) " + str)
  end
  
  def nick(str)
    @sock.name = str
  end
  
  def quit(str)
    @sock.disconnect()
  end
  
  def names(str)
    @sock.list_users
  end
  
end