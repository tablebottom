require 'tk'
require 'tkextlib/tile'
require 'ttsock'
require 'ttdefs'

class TTTk
  def initialize(host, room, username)
    @ttsock = TTSock.new(host, room, username)
    @ttcmd = TTCommander.new(@ttsock)
    
    root = TkRoot.new() { title "Tablebottom" }
    frame = Tk::Tile::Frame.new(root)
    frame.grid(:sticky => "nsew")
    TkGrid.columnconfigure root, 0, :weight => 1; TkGrid.rowconfigure root, 0, :weight => 1
    
    @tkout = TkText.new(frame) { width 80; height 20;}
    @tkout.grid( :row => 0, :column => 0, :sticky => "nsew")
    TkGrid.columnconfigure frame, 0, :weight => 1; TkGrid.rowconfigure frame, 0, :weight => 1
    
    @tkout.value = "Welcome to Tablebottom"
    
    @tkin = Tk::Tile::Entry.new(frame) { width 80; }
    @tkin.grid( :row => 1, :column => 0, :sticky => "nsew")
    @invar = TkVariable.new("")
    @tkin.textvariable(@invar)
    
    root.bind("Return") { return_pressed(@invar.value); @invar.value = "" }
    Tk.after(10) { do_network() }
  end
  
  def return_pressed(str)
    @ttcmd.xyzzy_do_cmd(str)
  end
  
  def out_line(str)
    @tkout.value = @tkout.value + str + "\n"
  end
  
  def do_network()
    msg = @ttsock.get_msg
    

    
    if msg and ((msg[0] == :lPresent) or (msg[0] == :mSay) or (msg[0] == :mOSay) or (msg[0] == :mCalc) or (msg[0] == :mRoll) or (msg[0] == :mFact) or (msg[0] == :mCheck))
      out_line(msg.to_s)
    end
    
    Tk.after(10) { do_network() }
  end
end

if __FILE__ == $0
  print "Host (None for Dare's system) : "
  host = gets.strip
  if host == ""
    host = TTDefs::HOST
  end
  
  print "Room : "
  room = gets.strip
  
  print "Name : "
  name = gets.strip
  
  foo = TTTk.new(host, room, name)
  
  Tk.mainloop()
end