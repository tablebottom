require 'stringio'
require 'stringio_utils'
require 'tabletoptwo_msgtypes'

module TabletopTwo

  # String -> (Symbol, [String])
  #
  # Unpacks a Tabletop2 message into an array containing one symbol, and an
  # array of strings
  def parse_msg(msgstr)
    strio = StringIO.new(msgstr)
    return [INTMSGTYPE[strio.getint()], lstr_array_unpack(strio)]
  end
  
  # StringIO -> [String]
  # 
  # Unpack an array of LStrs
  def lstr_array_unpack(strio)
    return [strio.readlstr()] + lstr_array_unpack(strio) unless strio.eof?
    return []
  end
  
end