require 'stringio'

# TruncatedDataError is raised when StringIO#readbytes fails to read enough data.

class TruncatedDataError<IOError
  def initialize(mesg, data) # :nodoc:
    @data = data
    super(mesg)
  end

  # The read portion of an StringIO#readbytes attempt.
  attr_reader :data
end

class StringIO
  # Reads exactly +n+ bytes.
  #
  # If the data read is nil an EOFError is raised.
  #
  # If the data read is too short a TruncatedDataError is raised and the read
  # data is obtainable via its #data method.
  def readbytes(n)
    str = read(n)
    if str == nil
      raise EOFError, "End of file reached"
    end
    if str.size < n
      raise TruncatedDataError.new("data truncated", str) 
    end
    str
  end
  
  # Uses readbytes to read 4 bytes, unpacks as a little-endian integer, and returns it
  def readint()
    return this.readbytes(4).unpack("v")[0]
  end
  
  # Uses readint and readbytes to read an lstr
  def readlstr()
    return this.readbytes(this.readint())
  end
  
end
