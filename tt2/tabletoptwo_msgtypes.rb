module TabletopTwo
  SYMMSGTYPE = {
    :mBugged    => 0x000,
    :mVersion   => 0x0EE,
    :mBatch     => 0x0FE
  }
  
  INTMSGTYPE = SYMMSGTYPE.invert
end